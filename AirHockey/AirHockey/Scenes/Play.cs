using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;

namespace AirHockey
{
    public class Play : Scene
    {
        SpriteFont font;

        Texture2D texBluePaddle;
        Texture2D texRedPaddle;
        Texture2D texPuck;

        Texture2D texEnemyGoal;
        Texture2D texPlayerGoal;

        Rectangle PlayerGoal;
        Rectangle EnemyGoal;

        Paddle player;
        Paddle enemy;

        Puck puck;

        private SceneManager sceneManager;

        public Play(SceneManager sceneManager, AirHockey game)
            : base(sceneManager, game)
        {
            // TODO: Complete member initialization
            this.sceneManager = sceneManager;

            int goalWidth = game.graphics.PreferredBackBufferHeight / 3;
            int goalHorizontalPosition = (game.graphics.PreferredBackBufferHeight - goalWidth) / 2;
            int goalHeight = game.graphics.PreferredBackBufferWidth / 40;

            PlayerGoal = new Rectangle(game.graphics.PreferredBackBufferWidth - goalHeight, goalHorizontalPosition, goalHeight, goalWidth);

            EnemyGoal = new Rectangle(0, goalHorizontalPosition, goalHeight, goalWidth);
        }

        public override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.

            // TODO: use this.Content to load your game content here
            texPlayerGoal = game.Content.Load<Texture2D>("PlayerGoal");
            texEnemyGoal = game.Content.Load<Texture2D>("EnemyGoal");
            texRedPaddle = game.Content.Load<Texture2D>("bluepaddle");
            texPuck = game.Content.Load<Texture2D>("Puck");
            texBluePaddle = game.Content.Load<Texture2D>("bluePaddle");
            font = game.Content.Load<SpriteFont>("Font");

            enemy = new Paddle(new Vector2(150, 200), texRedPaddle);
            player = new Paddle(new Vector2(600, 200), texBluePaddle);
            puck = new Puck(new Vector2(400, 200), texPuck);
        }

        new public void Unload()
        {
            texBluePaddle.Dispose();
        }

        public override void Update(GameTime gameTime)
        {
            handleInput(gameTime);
            handleCollisions(gameTime);

            player.Update(gameTime);
            enemy.Update(gameTime);
            puck.Update(gameTime);

            if (puck.Bounds.Intersects(EnemyGoal))
            {
                this.sceneManager.currentState = SceneManager.SceneState.Win;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();

            player.Draw(spriteBatch);
            enemy.Draw(spriteBatch);
            puck.Draw(spriteBatch);
            spriteBatch.DrawString(font, player.Bounds.ToString(), new Vector2(0, 0), Color.Red);
            spriteBatch.Draw(texEnemyGoal, EnemyGoal, Color.AliceBlue);
            spriteBatch.Draw(texPlayerGoal, PlayerGoal, Color.AliceBlue);
            spriteBatch.End();
        }

        private void handleInput(GameTime gameTime)
        {
            TouchCollection touchCollection = TouchPanel.GetState();

            if (touchCollection.Count > 0)
            {
                Point touchPoint = new Point((int)touchCollection[0].Position.X, (int)touchCollection[0].Position.Y);

                while (TouchPanel.IsGestureAvailable)
                {
                    GestureSample Gesture = TouchPanel.ReadGesture();

                    switch (Gesture.GestureType)
                    {
                        case GestureType.Hold:

                            break;

                        case GestureType.FreeDrag:
                            player.Velocity.X += Gesture.Delta.X;
                            player.Velocity.Y += Gesture.Delta.Y;
                            player.sprite.Center = Gesture.Position;
                            break;
                    }
                }
            }
        }

        public void handleCollisions(GameTime gameTime)
        {
            if (enemy.Bounds.Right > game.GraphicsDevice.Viewport.Bounds.Right || enemy.Bounds.Left > game.GraphicsDevice.Viewport.Bounds.Left)
            {
                enemy.Velocity.X = -enemy.Velocity.X;
            }

            else
            {
                enemy.Velocity.X += 5;
            }

            //if (puck.Bounds.Intersects(player.Bounds))
            if (puck.isCollided(player))
            {
                puck.Velocity += player.Velocity;
            }

            if (puck.Bounds.Intersects(enemy.Bounds))
            {
                puck.Velocity += enemy.Velocity;
            }

            if (player.Bounds.Right > game.GraphicsDevice.Viewport.Bounds.Right || player.Bounds.Left < game.GraphicsDevice.Viewport.Bounds.Left)
            {
                player.Velocity.X = -player.Velocity.X;
            }

            if (player.Bounds.Bottom > game.GraphicsDevice.Viewport.Bounds.Bottom || player.Bounds.Y < game.GraphicsDevice.Viewport.Bounds.Top)
            {
                player.Velocity.Y = -player.Velocity.Y;
            }

            if (puck.Bounds.Right > game.GraphicsDevice.Viewport.Bounds.Right || puck.Bounds.Left < game.GraphicsDevice.Viewport.Bounds.Left)
            {
                if (PlayerGoal.Intersects(puck.Bounds))
                {
                    sceneManager.currentState = SceneManager.SceneState.Win;
                }

                puck.Velocity.X = -puck.Velocity.X;
            }

            if (puck.Bounds.Bottom > game.GraphicsDevice.Viewport.Bounds.Bottom || puck.Bounds.Y < game.GraphicsDevice.Viewport.Bounds.Top)
            {
                puck.Velocity.Y = -puck.Velocity.Y;
            }
        }
    }
}