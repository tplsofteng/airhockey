using AirHockey.Scenes;

namespace AirHockey
{
    public class SceneManager
    {
        public enum SceneState { Start, Play, Win };

        public SceneState currentState;
        public SceneState prevState;
        public Scene currentScene;
        public AirHockey game;

        public SceneManager(AirHockey parent)
        {
            this.game = parent;
            currentState = SceneState.Play;
        }

        public Scene nextScene()
        {
            if (currentScene == null)
            {
                currentScene = new Play(this, game);
                currentScene.LoadContent();
            }

            switch (currentState)
            {
                case SceneState.Play:
                    if (prevState != currentState)
                    {
                        currentScene = new Play(this, game);
                        currentScene.LoadContent();
                        prevState = currentState;
                    }

                    break;

                case SceneState.Win:
                    if (prevState != SceneState.Win)
                    {
                        currentScene = new Win(this, game);
                        currentScene.LoadContent();
                        prevState = currentState;
                    }

                    break;

                default:
                    if (currentScene == null)
                    {
                        currentScene = new Play(this, game);
                    }
                    break;
            }

            return currentScene;
        }
    }
}