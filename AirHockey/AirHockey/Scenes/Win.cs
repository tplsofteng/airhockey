using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;

namespace AirHockey.Scenes
{
    public class Win : Scene
    {
        SpriteFont font;

        public Win(SceneManager parent, AirHockey game)
            : base(parent, game)
        {
        }

        public override void LoadContent()
        {
            font = game.Content.Load<SpriteFont>("Font");
        }

        public override void Update(GameTime gameTime)
        {
            if (TouchPanel.IsGestureAvailable)
            {
                sceneManager.currentState = SceneManager.SceneState.Play;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.DrawString(font, "Win", new Vector2(400, 400), Color.Black, -1.5f, new Vector2(0, 0), 3.0f, SpriteEffects.None, 0.0f);
            spriteBatch.End();
        }

        public override void Unload()
        {
        }
    }
}