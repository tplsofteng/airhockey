﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AirHockey
{
    public class Scene
    {
        public AirHockey game;
        public SceneManager sceneManager;

        public Scene(SceneManager parent, AirHockey game)
        {
            this.game = game;
            this.sceneManager = parent;
        }

        public virtual void LoadContent()
        {
        }

        public virtual void Update(GameTime gameTime)
        {
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
        }

        public virtual void Unload()
        {
        }
    }
}