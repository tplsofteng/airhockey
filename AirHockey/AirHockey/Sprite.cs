using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AirHockey
{
    internal class Sprite
    {
        public Vector2 Center;
        public Texture2D texture;

        public Sprite(Texture2D texture)
        {
            this.texture = texture;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(
                texture,
                Center,
                null,
                Color.AliceBlue,
                0,
                new Vector2(texture.Width / 2, texture.Height / 2),
                1,
                SpriteEffects.None,
                0);
        }
    }
}