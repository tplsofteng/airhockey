using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AirHockey
{
    internal class Puck : Character
    {
        public Vector2 Velocity;
        public float maxVelocity = 10.0f;
        public float Friction = 0.9f;
        public float radius = 0.0f;

        public Puck(Vector2 position, Texture2D texture)
            : base(position, texture)
        {
            Velocity = Vector2.Zero;
            shape = Shape.Circular;
        }

        public void Update(GameTime gameTime)
        {
            Velocity *= 1f - (Friction * (float)gameTime.ElapsedGameTime.TotalSeconds);

            sprite.Center += Velocity * (float)gameTime.ElapsedGameTime.TotalSeconds;
        }
    }
}