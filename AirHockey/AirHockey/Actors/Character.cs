using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AirHockey
{
    public enum Shape { Square, Rectangular, Circular };

    internal class Character
    {
        public Shape shape;

        public Rectangle Bounds
        {
            get
            {
                return new Rectangle(
                    (int)sprite.Center.X - sprite.texture.Width / 2,
                    (int)sprite.Center.Y - sprite.texture.Height / 2,
                    sprite.texture.Width,
                    sprite.texture.Height);
            }

            set
            {
            }
        }

        public Sprite sprite;

        public Character()
        {
            shape = Shape.Rectangular;
            Bounds = new Rectangle();
        }

        public Character(Vector2 position, Texture2D texture)
        {
            this.sprite = new Sprite(texture);
            this.sprite.Center = new Vector2(position.X + sprite.texture.Width / 2, position.Y + sprite.texture.Height / 2);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            sprite.Draw(spriteBatch);
        }

        public bool isCollided(Character GameChar)
        {
            bool retVal = false;

            if (GameChar.shape == Shape.Circular && this.shape == Shape.Circular)
            {
                int thisRadius = Math.Abs(this.Bounds.Center.Y - this.Bounds.Left);
                int otherRadius = Math.Abs(GameChar.Bounds.Center.Y - GameChar.Bounds.Left);

                if ((GameChar.Bounds.Center.X - this.Bounds.Center.X) * (GameChar.Bounds.Center.X - this.Bounds.Center.X) + (GameChar.Bounds.Center.Y - this.Bounds.Center.Y) * (GameChar.Bounds.Center.Y - this.Bounds.Center.Y) < (thisRadius + otherRadius))
                {
                    retVal = true;
                }
            }
            return retVal;
        }
    }
}